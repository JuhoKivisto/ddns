from pathlib import Path

__all__ = [                 \
    "get_project_root",     \
    "get_config_ini_path",  \
    "get_auth_ini_path",    \
    "get_ipfile_path",      \
    "get_logfile_path",     \
]

def get_project_root() -> Path:
    return Path(__file__).parent.parent

def get_config_ini_path() -> Path:
    return get_project_root().joinpath("cfg/config.ini")

def get_auth_ini_path() -> Path:
    return get_project_root().joinpath("cfg/auth.ini")

def get_ipfile_path() -> Path:
    return get_project_root().joinpath("cfg/history.json")

def get_logfile_path() -> Path:
    return get_project_root().joinpath("ddns.log")
