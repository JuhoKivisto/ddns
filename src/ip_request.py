#!/usr/bin/python3
import urllib3
import configparser
import json
import os
import datetime

from configfiles import *

# TODO: setup.py. Requires urllib3

HISTORYFILE_IP_TEXT         = "ip"
HISTORYFILE_TIMESTAMP_TEXT  = "timestamp"


class IPRequest:
    """
    This program is used to determine if the public IP address has changed and to update the 
    IP address for a DDNS (dy.fi).
    Also if the IP does not change, this program will post a request for the DDNS periodically 
    (e.g. about once a week)

    This script does not run as daemon. It works so that this script is run e.g. once an hour
    and it will store all the neccessary information to configuration files (for example the 
    timestamp of last update to the DDNS, or the last public IP address it had.

    The basic structure is that first it reads the configuration files, then sends a get-request 
    to check the current IP-address and compares it with the IP address which was got the last 
    time. If those differ, a new request has to be made.

    The program also checks the timestamp of last update; if the timedelta is too big, a new 
    update request is sent.

    Note! This program needs an "auth.ini" -file which holds the login credentials for dy.fi.
    

    """

    __config            = None
    __config_auth       = None
    __refresh_time      = None # 432000s = 5d
    __last_request_ip   = None
    __last_request_time = None
    __new_request_ip    = None
    __ip_check_url      = None 
    __ip_update_url     = None 
    __hostnames         = None    

    __loginname         = None
    __password          = None

    __fname_config      = get_config_ini_path()
    __fname_auth        = get_auth_ini_path()
    __fname_ipfile      = get_ipfile_path()
    __logfile           = get_logfile_path()

    __http = urllib3.PoolManager()


    def __init__(self):
        self.__config = configparser.ConfigParser()
        self.__config.read(self.__fname_config)

        self.__config_auth = configparser.ConfigParser()
        self.__config_auth.read(self.__fname_auth)

        # These are read from auth.ini, please create it with your credentials!
        self.__loginname            = self.__config_auth['dy.fi']['user']
        self.__password             = self.__config_auth['dy.fi']['password']
        
        self.__ip_check_url         = self.__config['URLs']['checkip_url']
        self.__ip_update_url        = self.__config['URLs']['update_url']
        self.__hostnames            = self.__config['URLs']['hostname'].split(",")

        for hostname_index in range(len( self.__hostnames)):
            self.__hostnames[hostname_index] = self.__hostnames[hostname_index].strip()
                                 
        rtime = int(self.__config['URLs']['refresh_time_seconds'])
        self.__refresh_time         = datetime.timedelta(seconds=rtime)

 
        try:
            historydata = json.load(open(self.__fname_ipfile))
        except FileNotFoundError:
            pass
        else:
            if HISTORYFILE_IP_TEXT in historydata and HISTORYFILE_TIMESTAMP_TEXT in historydata:
                self.__last_request_ip   = historydata[HISTORYFILE_IP_TEXT]
                self.__last_request_time = datetime.datetime.strptime(
                    historydata[HISTORYFILE_TIMESTAMP_TEXT], "%Y-%m-%d %H:%M:%S")


        self.__new_request_ip = self._request_ip()


    def _request_ip(self):
        """
        Update IP address by making a HTTP request. 
        Makes a new request if the refresh time has been passed.
        Stores the IP and a timestamp into a file.

        :param: None
        :return: str if a valid request could be made, None if failed (i.e. no connection)
        """

        try:
            r = self.__http.request('GET', self.__ip_check_url)
        except:
            logfile.write("Failed to check current IP.\n")
            return None
        else:
            my_ip_addr = None
            if r.status == 200:
                response_str = r.data.decode('UTF-8')

            return response_str

        # TODO: If no reasonable response is gotten from the server, what to do? 
        # Send a update neverthaless?
        return None


    def has_changed(self):
        """ 
        Probes if IP address has changed since last call with a successful request or if
        there  are no signs from previous updates

        :param: None
        :return: bool whether ip should be updated now
        """

        # If IP request failed, it may have been changed. 
        # Better to still send new update to DDNS.

        if self.__new_request_ip is None or         \
                self.__last_request_ip is None or   \
                self.__last_request_time is None:
            return True

        # IP has changed, a new update request needs to be made
        return self.__new_request_ip != self.__last_request_ip

    
    def has_expired(self):
        """
        Compares the timedelta from the last published IP and the time now to the update frequency
        :param: None
        :return: Bool; If last publication time has expired, True. Otherwise False.
        """

        time_now = datetime.datetime.now()

        if self.__last_request_time is None:
            return True
        return time_now - self.__last_request_time > self.__refresh_time


    def publish_ip(self):
        """
        :brief: Creates a new HTTPS POST request which updates the lease in DDNS.
            Logs event into logfile.
        :param: None
        :return: None
        """
        headers = urllib3.make_headers(basic_auth="{}:{}"\
                .format(self.__loginname, self.__password))

        for hostname in self.__hostnames:
            print(hostname)
            r = self.__http.request('POST', "{}?hostname={}"\
                .format(self.__ip_update_url, hostname), headers=headers)

        time_now = datetime.datetime.now()
        if r.status == 200:
            historydict = {
                    HISTORYFILE_IP_TEXT: self.__new_request_ip, 
                    HISTORYFILE_TIMESTAMP_TEXT: datetime.datetime.strftime(time_now, "%Y-%m-%d %H:%M:%S")
            }
            with open(self.__fname_ipfile, 'w') as ipfile:
                ipfile.write(json.dumps(historydict, sort_keys=True, indent=4))

            with open(self.__logfile, 'a') as logfile:
                logfile.write("IP published at {}\n".format(time_now))
        
        else:
            with open(self.__logfile, 'a') as logfile:
                logfile.write("Failed to publish IP; status={}".format(r.status))